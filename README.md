# Base Documentaire

Projet : Romain GEFFROY - Alexandre MOURIEC

## Procédure d'installation

### Cloner le repo

```shell
git clone https://gitlab.com/mrcalexandre/projet-gestion-serveur.git
cd projet-gestion-serveur
```

### Lancer le docker compose

```shell
docker-compose up --build
```

Cela va lancer le docker-compose en même temps que le build.

Visitez ensuite `http://localhost:4200/`



# Procédure de test

Pour tester ce projet, il faut visiter `/signup.html` pour créer un compte puis `/login.html` pour se connecter

Si vous tentez de visiter `/pageProtegee.html` sans être connecté, vous serez redirigés vers la page login. Sinon la page sera affichée comme prévu.