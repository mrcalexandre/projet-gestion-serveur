var mysql = require("mysql");
var express = require("express");
var session = require("express-session");
var bodyParser = require("body-parser");
var path = require("path");

var connection = mysql.createConnection({
  host: "localhost",
  user: "admin",
  password: "romainalexandre",
  database: "bdd"
});

var app = express();

app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true
  })
);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", function(request, response) {
  response.sendFile(path.join(__dirname + "/login.html"));
});

app.get("/register", function(request, response) {
    response.sendFile(path.join(__dirname + "/signup.html"));
  });

app.post("/auth", function(request, response) {
  var username = request.body.username;
  var password = request.body.password;
  if (username && password) {
    connection.query(
      "SELECT * FROM User WHERE username = ? AND password = ?",
      [username, password],
      function(error, results, fields) {
        if (results.length > 0) {
          request.session.loggedin = true;
          request.session.username = username;
          response.redirect("/home");
        } else {
          response.send("Identifiant ou mot de passe incorrect");
        }
        response.end();
      }
    );
  } else {
    response.send("Veuillez entrer votre identifiant ou mot de passe");
    response.end();
  }
});

app.get("/home", function(request, response) {
  if (request.session.loggedin) {
  } else {
    response.send("Connectez vous!");
  }
  response.end();
});

app.post("/signup", function(req, res) {
  const body = req.body;

  db.query("SELECT * FROM User", function(err, result) {
    result = JSON.parse(JSON.stringify(result));
    db.query(
      "INSERT INTO `User`(username, password) VALUES(?, ?)",
      [body.username, body.password],
      function(err, result) {
        if (err) console.log("Erreur d'insertion" + err);
        res.json("Vous êtes inscrits!");
      }
    );
  });
});

app.listen(8080, function() {
  console.log("Le serveur est disponible sur le port " + 8080);
});
